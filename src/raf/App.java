package raf;

import java.io.IOException;

public class App {
	static Persistencia p;

	public static void main(String[] args) {
		try {
			p = new Persistencia("clientes.dat");
//			rellenaFichero();

//			p.irFinal();
//			Cliente cli = new Cliente((short) 501,"Sergio","Lo que sea", 1000f);
//			p.guardar(cli);

//			p.irRegistro(2);
//			System.out.println(p.leer());

//			p.borrarRegistro();

//			muestraFichero();

//			Cliente cli1 = new Cliente((short) 1, "clinombre1", "cliapellido1", (float) 100.1);
//			Cliente cli = p.buscarID((short) 1);
//			if (cli != null) {
//				System.out.println("Encontrado: " + cli1);
//				System.out.println("Encontrado: " + cli);
//				if (cli.equals(cli1))
//					System.out.println("iguales");
//				else
//					System.out.println("diferentes");
//			} else {
//				System.out.println("No existe o está borrado");
//			}

			muestraFichero();
			System.out.println("Huecos compactados: " + p.compactar1());
			muestraFichero();

		} catch (IOException e) {
			System.out.println("Error: " + e.getMessage());
		}

	}

	private static void muestraFichero() throws IOException {
		Cliente cli;
		p.irInicio();
		for (int i = 0; i < p.totalRegistros(); i++) {
			cli = p.leer();
//			if (!cli.estaBorrado()) {
			System.out.println(cli);
//			}
		}
		System.out.println("Total registros: " + p.totalRegistros());
	}

	private static void rellenaFichero() {
		try {
			for (int i = 1; i <= 1000; i++) {
				int a = ((short) Math.round(Math.random())) == 1 ? i * (-1) : i;
				Cliente cli = new Cliente((short) a, "nomcliente" + i, "apellidoscliente" + i,
						(float) Math.random() * 1000);
				p.guardar(cli);
			}
		} catch (IOException e) {
			System.out.println("Error rellenando fichero: " + e.getMessage());
		}

	}

}
