package tests;

import static org.junit.Assert.assertNull;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.junit.jupiter.api.Assertions.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Order;
import org.junit.jupiter.api.Test;

import raf.Cliente;
import raf.Persistencia;

class TestPersistencia {
	static Cliente c1, c2, c3, c4, c5, c6, c7, c8;
	static Persistencia persistenciaTest1, persistenciaTest2;

	final static String NOMBRE_ARCHIVO_TEST1 = "clientes1.dat";
	final static String NOMBRE_ARCHIVO_TEST2 = "clientes.dat";
	final static String NOMBRE_ARCHIVO_TEST2_BAK = "clientes.bak";
	final static String NOMBRE_ARCHIVO_TEST2_RESULTADO = "compactado.dat";

	@BeforeAll
	static void setUpBeforeClass() {

		// Preparación test1
		File archivoTest1 = new File(NOMBRE_ARCHIVO_TEST1);
		if (archivoTest1.exists()) {
			archivoTest1.delete();
		}
		c1 = new Cliente((short) 1, "clinombre1", "cliapellido1", 100.1f);
		c2 = new Cliente((short) 2, "clinombre2", "cliapellido2", 200.2f);
		c3 = new Cliente((short) -3, "clinombre3", "cliapellido3", 300.3f);
		c4 = new Cliente((short) -4, "clinombre4", "cliapellido4", 400.4f);
		c5 = new Cliente((short) 5, "clinombre5", "cliapellido5", 500.5f);
		c6 = new Cliente((short) -6, "clinombre6", "cliapellido6", 600.6f);
		c7 = new Cliente((short) 7, "clinombre7", "cliapellido7", 700.7f);
		c8 = new Cliente((short) -8, "clinombre8", "cliapellido8", 800.8f);
		try {
			persistenciaTest1 = new Persistencia(NOMBRE_ARCHIVO_TEST1);
			persistenciaTest1.guardar(c1);
			persistenciaTest1.guardar(c2);
			persistenciaTest1.guardar(c3);
			persistenciaTest1.guardar(c4);
			persistenciaTest1.guardar(c5);
			persistenciaTest1.guardar(c6);
			persistenciaTest1.guardar(c7);
			persistenciaTest1.guardar(c8);
		} catch (IOException e) {
			fail("El test falla al preparar fichero de pruebas - test1");
		}

		// Preparación test2
		File archivoTest2 = new File(NOMBRE_ARCHIVO_TEST2);
		File archivoTest2bak = new File(NOMBRE_ARCHIVO_TEST2_BAK);
		try {
			if (archivoTest2.exists())
				archivoTest2.delete();
			Files.copy(archivoTest2bak.toPath(), archivoTest2.toPath());
			persistenciaTest2 = new Persistencia(NOMBRE_ARCHIVO_TEST2);
		} catch (IOException e) {
			fail("El test falla al preparar fichero de pruebas - test2");
		}

	}

	@Test
	@Order(1)
	void testBuscarClientePorID() {
		Cliente buscar1 = c1;
		Cliente buscar2 = c7;
		try {
			assertEquals(buscar1, persistenciaTest1.buscarID((short) 1));
			assertEquals(buscar2, persistenciaTest1.buscarID((short) 7));
			assertNull(persistenciaTest1.buscarID((short) 4));
			assertNull(persistenciaTest1.buscarID((short) 9));
		} catch (IOException e) {
			fail("El test falla durante test1: buscar por id");
		}
	}

	@Test
	@Order(2)
	void testCompactarFichero() {
		File archivoTest2 = new File(NOMBRE_ARCHIVO_TEST2);
		File archivoTest2bak = new File(NOMBRE_ARCHIVO_TEST2_BAK);
		File archivoTest2Resultado = new File(NOMBRE_ARCHIVO_TEST2_RESULTADO);
		try {
			long compactados = persistenciaTest2.compactar1() * Cliente.TAM_REGISTRO;			
			assertEquals(archivoTest2bak.length() - archivoTest2Resultado.length(), compactados);
			assertEquals(archivoTest2Resultado.length(), archivoTest2.length());
		} catch (IOException e) {
			fail("El test falla durante test2: compartar");
		}

	}

}
